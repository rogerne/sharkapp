class AddSerializationToShark < ActiveRecord::Migration[5.2]
  def change
    add_column :sharks, :serialized_adder, :text
  end
end
