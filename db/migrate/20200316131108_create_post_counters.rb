class CreatePostCounters < ActiveRecord::Migration[5.2]
  def change
    create_table :post_counters do |t|
      t.integer :count

      t.timestamps
    end
  end
end
