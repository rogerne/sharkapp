require "rails_helper"

RSpec.describe PostCounter, :type => :model do

	before(:context) do
    @shark = Shark.create!(name: "White", facts: "Big")
  end

  context "when I create a new Shark" do
    it "tells me there are 0 Posts" do
    	puts "Shark name = #{@shark.name}"
      expect(@shark.number_of_posts).to eq 0
    end
  end
end