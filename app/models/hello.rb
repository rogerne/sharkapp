class Hello

  def initialize(str)
    @str = str
  end

  def sayHello
    @str
  end
end

o = Hello.new("Hello World\n")
data = Marshal.dump(o)
obj = Marshal.load(data)
puts data
puts obj.sayHello 