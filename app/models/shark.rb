class Shark < ApplicationRecord
  has_many :posts, dependent: :destroy
	validates :name, presence: true, uniqueness: true
	validates :facts, presence: true

	#after_initialize :init

	#http://phrogz.net/programmingruby/ref_m_marshal.html

 
	before_save() do
		unless new_record? 
			#self.number_of_posts += 1
			@mc = Marshal.load(serialized_adder)
		else
			init
		end


			@mc.increment_count
			self.number_of_posts = @mc.sum_of_count
			self.serialized_adder = Marshal.dump(@mc)		
	end

	def init
		#self.number_of_posts ||= 0
		@mc = MyCounter.new
	end
	
	def to_s
		return "This shark is called #{self.name} and my facts are #{self.facts}"
	end
end
