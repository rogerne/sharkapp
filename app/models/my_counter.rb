class MyCounter

	attr_accessor :sum_of_count

	def initialize
		@sum_of_count ||= 0
	end

	def increment_count
		@sum_of_count += 2
	end
end