class PostsController < ApplicationController
  before_action :get_shark
  before_action :set_post, only: [:show, :edit, :update, :destroy]


  # GET /posts
  # GET /posts.json
  def index
    puts "------------------Hello Mum"
    #@posts = Post.all
    #@shark.posts.each {|x| puts "What have we got ... #{x.text}"}
    puts "The shark is #{@shark}"
    @posts = @shark.posts

    
  end

  # GET /posts/1
  # GET /posts/1.json
  def show
  end

  # GET /posts/new
  def new
    puts "New sire?..............."
    #@post = Post.new
    @post = @shark.posts.build
  end

  # GET /posts/1/edit
  def edit
  end

  # POST /posts
  # POST /posts.json
  def create
    #@post = Post.new(post_params)
    @post = @shark.posts.build(post_params)

    respond_to do |format|
      if @post.save
        #format.html { redirect_to @post, notice: 'Post was successfully created.' }
        format.html { redirect_to shark_posts_path(@shark), notice: 'Post was successfully created.' }
        format.json { render :show, status: :created, location: @post }
      else
        format.html { render :new }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /posts/1
  # PATCH/PUT /posts/1.json
  def update
    respond_to do |format|
      if @post.update(post_params)
        #format.html { redirect_to @post, notice: 'Post was successfully updated.' }
        format.html { redirect_to shark_post_path(@shark), notice: 'Post was successfully updated.' }
        format.json { render :show, status: :ok, location: @post }
      else
        format.html { render :edit }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /posts/1
  # DELETE /posts/1.json
  def destroy
    @post.destroy
    respond_to do |format|
      #format.html { redirect_to posts_url, notice: 'Post was successfully destroyed.' }
      format.html { redirect_to shark_posts_path(@shark), notice: 'Post was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
   #This method will create a local @shark instance variable by finding a shark instance by shark_id. 
   #With this variable available to us in the file, it will be possible to relate posts to a specific shark in the other methods.
    def get_shark
      puts "Getting a Shark for id #{params[:shark_id]}"
      @shark = Shark.find(params[:shark_id])
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_post
      #@post = Post.find(params[:id])
       @post = @shark.posts.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def post_params
      params.require(:post).permit(:body, :shark_id)
    end
end
